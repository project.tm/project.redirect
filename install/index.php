<?php

use Bitrix\Main\ModuleManager,
    Bitrix\Main\EventManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Project\Redirect\Model\RedirectTable;

IncludeModuleLangFile(__FILE__);

class project_redirect extends CModule {

    public $MODULE_ID = 'project.redirect';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_REDIRECT_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_REDIRECT_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_REDIRECT_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->InstallDB();
        $this->InstallEvent();
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallDB();
        $this->UnInstallEvent();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    /*
     * InstallDB
     */

    public function InstallDB() {
        RedirectTable::tableCreate();
    }

    public function UnInstallDB() {
        RedirectTable::tableDrop();
    }

    /*
     * InstallEvent
     */

    public function InstallEvent() {
        $eventManager = EventManager::getInstance();
        $eventManager->registerEventHandler('main', 'OnPageStart', $this->MODULE_ID, '\Project\Redirect\Event\Redirect', 'OnPageStart');
    }

    public function UnInstallEvent() {
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler('main', 'OnPageStart', $this->MODULE_ID, '\Project\Redirect\Event\Redirect', 'OnPageStart');
    }

}
