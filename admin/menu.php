<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Localization\Loc,
    Project\Redirect\Redirect\AdminInterface\RedirectEditHelper,
    Project\Redirect\Redirect\AdminInterface\RedirectListHelper;

if (!Loader::includeModule('digitalwand.admin_helper') || !Loader::includeModule('project.redirect'))
    return;

Loc::loadMessages(__FILE__);

return array(
    array(
        'parent_menu' => 'global_menu_services',
        'sort' => 350,
        'icon' => 'fileman_sticker_icon',
        'page_icon' => 'fileman_sticker_icon',
        'text' => Loc::getMessage('PROJECT_REDIRECT_MENU'),
        'url' => RedirectListHelper::getUrl(),
        'more_url' => array(
            RedirectEditHelper::getUrl()
        )
    )
);
