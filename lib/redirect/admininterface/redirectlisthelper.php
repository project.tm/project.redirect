<?php

namespace Project\Redirect\Redirect\AdminInterface;

use DigitalWand\AdminHelper\Helper\AdminListHelper;

/**
 * Хелпер описывает интерфейс, выводящий список новостей.
 *
 * {@inheritdoc}
 */
class RedirectListHelper extends AdminListHelper {

    protected static $model = 'Project\Redirect\Model\RedirectTable';

}
